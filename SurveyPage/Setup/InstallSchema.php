<?php

namespace Survey\SurveyPage\Setup;

use \Magento\Framework\Setup\InstallSchemaInterface;
use \Magento\Framework\Setup\ModuleContextInterface;
use \Magento\Framework\Setup\SchemaSetupInterface;
use \Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 *
 * @package Toptal\Blog\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * Install Blog Posts table
     *
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $tableName = $setup->getTable('Survey_answers');

        if ($setup->getConnection()->isTableExists($tableName) != true) {
            $table = $setup->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'post_id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Name'
                )
                ->addColumn(
                    'age',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Age'
                )
                ->addColumn(
                    'answer1',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Created At'
                )
                ->addColumn(
                    'answer2',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Created At'
                )
                ->addColumn(
                    'answer3',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Created At'
                )
                ->setComment('Something comment ');
            $setup->getConnection()->createTable($table);
        }

        $setup->endSetup();
    }
}