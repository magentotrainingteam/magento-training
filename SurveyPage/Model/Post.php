<?php

namespace Survey\SurveyPage\Model;

use \Magento\Framework\Model\AbstractModel;
use \Magento\Framework\DataObject\IdentityInterface;
use \Survey\SurveyPage\Api\Data\PostInterface;

/**
 * Class File
 * @package Toptal\Blog\Model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Post extends AbstractModel implements PostInterface, IdentityInterface
{
    /**
     * Cache tag
     */
    const CACHE_TAG = 'Survey_answers';

    /**
     * Post Initialization
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Survey\SurveyPage\Model\ResourceModel\Post');
    }


    /**
     * Get Title
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->getData(self::NAME);
    }

    /**
     * Get Content
     *
     * @return string|null
     */
    public function getAge()
    {
        return $this->getData(self::AGE);
    }


    public function getAnswer1()
    {
        return $this->getData(self::ANSWER1);
    }


    public function getAnswer2()
    {
        return $this->getData(self::ANSWER2);
    }

    public function getAnswer3()
    {
        return $this->getData(self::ANSWER3);
    }
    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->getData(self::POST_ID);
    }

    /**
     * Return identities
     * @return string[]
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function setName($name)
    {
        return $this->setData(self::NAME, $name);
    }


    public function setAge($age)
    {
        return $this->setData(self::AGE, $age);
    }

    public function setAnswer1($answer1)
    {
        return $this->setData(self::ANSWER1, $answer1);
    }

    public function setAnswer2($answer2)
    {
        return $this->setData(self::ANSWER2, $answer2);
    }

    public function setAnswer3($answer3)
    {
        return $this->setData(self::ANSWER3, $answer3);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::POST_ID, $id);
    }
}