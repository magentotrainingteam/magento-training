<?php

namespace Survey\SurveyPage\Api\Data;

interface PostInterface
{
    /**#@+
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const POST_ID               = 'post_id';
    const NAME                  = 'name';
    const AGE                   = 'age';
    const ANSWER1               = 'answer1';
    const ANSWER2               = 'answer2';
    const ANSWER3               = 'answer3';

    /**#@-*/


    /**
     * Get Name
     *
     * @return string|null
     */
    public function getName();

    /**
     * Get Age
     *
     * @return string|null
     */
    public function getAge();

    /**
     * Get Answer1
     *
     * @return string|null
     */
    public function getAnswer1();


    /**
     * Get Answer2
     *
     * @return string|null
     */
    public function getAnswer2();

    /**
     * Get Answer3
     *
     * @return string|null
     */
    public function getAnswer3();

    /**
     * Get ID
     *
     * @return int|null
     */
    public function getId();

    /**
     * Set Name
     *
     * @param string $title
     * @return $this
     */
    public function setName($name);

    /**
     * Set Age
     *
     * @param string $content
     * @return $this
     */
    public function setAge($age);

    /**
     * Set Answer1
     *
     * @param string $answer1
     * @return $this
     */
    public function setAnswer1($answer1);

    /**
     * Set Answer2
     *
     * @param string $answer2
     * @return $this
     */
    public function setAnswer2($answer2);

    /**
     * Set Answer3
     *
     * @param string $answer3
     * @return $this
     */
    public function setAnswer3($answer3);

    /**
     * Set ID
     *
     * @param int $id
     * @return $this
     */
    public function setId($id);
}